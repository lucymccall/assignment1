using Microsoft.VisualStudio.TestTools.UnitTesting;
using assignment1;
using System;

namespace assignment1Test
{
    [TestClass]
    public class UnitTests
    {
        /// <summary>
        /// Test set var
        /// </summary>
        /// This is testing wether the custom variables that the user is defining are being set correctly.
        /// It does this by setting variables, and testing the expected outcome against the actual outcome.
        /// The test passes if the expected and actual outputs are the same.
        [TestMethod]
        public void TestSetVar()
        {
            //create objects of drawing class and command factory
            var drawclass = new Drawing(); 
            CommandFactory commandfactory = new CommandFactory();

            int expectedvarval1 = 65; //what the first var value should be after executing the command
            int expectedvarval2 = 35; //what the second var value should be after executing the command

            //create the command object, set values and execute command for first variable
            SetVar s = (SetVar)commandfactory.getCommand("setvar");
            s.Set(drawclass, "num1", 65);
            s.Execute();

            //create the command object, set values and execute command for second variable
            SetVar s2 = (SetVar)commandfactory.getCommand("setvar");
            s.Set(drawclass, "num2", 35);
            s.Execute();

            //retrieve the actual values of variables from drawclass dict
            int actualvarval1 = drawclass.dict["num1"];
            int actualvarval2 = drawclass.dict["num2"];

            //compare the expected variable values against the actual values
            Assert.AreEqual(expectedvarval1, actualvarval1, ("pen x coordinate position not set correctly")); //compare the expected and actual xPos positions.
            Assert.AreEqual(expectedvarval2, actualvarval2, ("pen y coordinate position not set correctly")); //compare the expected and actual yPos positions.

        }

        /// <summary>
        /// Tests whether an exception is thrown when incorrect command is entered into the command box
        /// </summary>
        /// This passes an incorrect command to the parseProgram method in Parser class, which should throw a FormatException.
        /// If the correct exception is thrown, then the test passes.
        [TestMethod]
        public void TestLoopCountException()
        {
            //create object of parser class
            var parseclass = new Parser();

            //Test whether correct exception is thrown
            Assert.ThrowsException<FormatException>(delegate () { parseclass.parseProgram("loop test\nrectangle 20 20\nendloop"); });
        }

        /// <summary>
        /// Tests whether an exception is thrown when incorrect command is entered into the command box
        /// </summary>
        /// This passes an incorrect command to the ParseAction method in Parser class, which should throw an ArgumentException.
        /// If the correct exception is thrown, then the test passes.
        [TestMethod]
        public void TestParserActionException()
        {
            //create object of parser class
            var parseclass = new Parser();

            //Test whether correct exception is thrown
            Assert.ThrowsException<ArgumentException>(delegate () { parseclass.ParseAction("hello"); });
        }

        /// <summary>
        /// Tests whether an exception is thrown when incorrect command is entered into the program
        /// </summary>
        /// This passes an incorrect command to the ParseProgram method in Parser class, which should throw an ArgumentException.
        /// If the correct exception is thrown, then the test passes.
        [TestMethod]
        public void TestParserProgramException()
        {
            //create object of parser class
            var parseclass = new Parser();

            //Test whether correct exception is thrown
            Assert.ThrowsException<ArgumentException>(delegate () { parseclass.parseProgram("hello"); });
        }

        /// <summary>
        /// Tests whether an exception is thrown when incorrect command is entered into the program
        /// </summary>
        /// This passes an incorrect command to the ParseProgram method in Parser class, which should throw an ArgumentException.
        /// If the correct exception is thrown, then the test passes.
        [TestMethod]
        public void TestIncorrectFactoryShapeException()
        {
            //create objects of parser class and command factory
            var parseclass = new Parser();
            ShapeFactory shapefactory = new ShapeFactory();

            //Test whether correct exception is thrown
            Assert.ThrowsException<ArgumentException>(delegate () { shapefactory.getShape("test"); });
        }

        /// <summary>
        /// Tests whether an exception is thrown when incorrect command is entered into the program
        /// </summary>
        /// This passes an incorrect command to the ParseProgram method in Parser class, which should throw an ArgumentException.
        /// If the correct exception is thrown, then the test passes.
        [TestMethod]
        public void TestIncorrectFactoryCommandException()
        {
            //create objects of parser class and command factory
            var parseclass = new Parser();
            CommandFactory commandfactory = new CommandFactory();

            //Test whether correct exception is thrown
            Assert.ThrowsException<ArgumentException>(delegate () { commandfactory.getCommand("test"); });
        }
    }
}
