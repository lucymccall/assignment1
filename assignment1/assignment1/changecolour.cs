﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace assignment1
{
    public class ChangeColour : Command
    {
        /// <summary>
        /// Change pen colour
        /// </summary>
        /// 
        /// This takes the following param as input:
        /// 
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="colour">The pen colour</param>
        /// 
        ///It then uses if statements to check wether the param pencolour is equal to any of the colours.
        ///If equal to any of the colours listed, it sets the pen and brush from drawing class to the chosen colour,
        ///so its ready to be passed into other methods to create shapes.
        /////An exception is thrown if pencolour param is not equal to any of the colours specified.

        //empty variables
        Drawing drawingclass;
        string colour;
        
        /// <summary>
        /// Sets the variables
        /// </summary>
        /// <param name="drawing">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawing, params object[] commands)
        {
            this.drawingclass = drawing; //sets the empty drawingclass to equal Drawing
            this.colour = Convert.ToString(commands[0]); //converts the object into a string
        }

        /// <summary>
        /// execute the command
        /// </summary>
        public override void Execute()
        {
            //if the string is equal to colour, change pen and brush colour
            if (colour.Contains("red"))
            {
                drawingclass.myPen.Color = Color.Red; //sets pen to red
                drawingclass.myBrush = new SolidBrush(Color.Red); //create new brush of colour red
            }
            else if (colour.Contains("blue"))
            {
                drawingclass.myPen.Color = Color.Blue; //sets pen to blue
                drawingclass.myBrush = new SolidBrush(Color.Blue); //create new brush of colour blue
            }
            else if (colour.Contains("yellow"))
            {
                drawingclass.myPen.Color = Color.Yellow; //sets pen to yellow
                drawingclass.myBrush = new SolidBrush(Color.Yellow); // Create new brush of colour yellow
            }
            else if (colour.Contains("black"))
            {
                drawingclass.myPen.Color = Color.Black; //sets pen to black
                drawingclass.myBrush = new SolidBrush(Color.Black); //create new brush of colour black
            }
            else
            {
                throw new InvalidParameterException();
            }
        }

    }
}
