﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public class UpdateVar : Command
    {
        /// <summary>
        /// Updates variables in a dictionary from Drawing class
        /// </summary>
        ///         /// <param name="varname">The variable name</param>
        /// <param name="oldvalue">GThe old variable value</param>
        /// <param name="updatedvalue">GThe updated variable value</param>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// Updates the variables setting the new value after performing a calcuation, so the new value can be accessed later to be used in different 
        /// shapes and commands.

        //empty variables
        string varname;
        int oldvalue;
        int updatedvalue;
        Drawing drawingclass;

        /// <summary>
        /// sets the correct params for updating a variable
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            try
            {
                //set local variables as passed in params
                this.varname = (string)commands[0];
                this.oldvalue = drawingclass.dict[varname];
                this.drawingclass = drawingclass;

                if (commands[1].Equals("+"))
                {
                    this.updatedvalue = oldvalue + Convert.ToInt32(commands[2]);
                }
                else if (commands[1].Equals("-"))
                {
                    this.updatedvalue = oldvalue - Convert.ToInt32(commands[2]);
                }
                else if (commands[1].Equals("*"))
                {
                    this.updatedvalue = oldvalue * Convert.ToInt32(commands[2]);
                }
                else if (commands[1].Equals("/"))
                {
                    this.updatedvalue = oldvalue / Convert.ToInt32(commands[2]);
                }
            } catch (Exception)
            {
                throw new InvalidParameterException();
            }


        }
        /// <summary>
        /// executes command
        /// </summary>
        public override void Execute()
        {
            drawingclass.dict[varname] = updatedvalue;
        }
    }
}
