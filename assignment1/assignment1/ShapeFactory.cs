﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public class ShapeFactory
    {
        /// <summary>
        /// The shape factory
        /// </summary>
        /// This class returns the objects that are used in parser class to set and execute each shape.
        /// getShape method is used with the shape name, in order to return that specific shape.
        /// <param name="shapeType">The type of command</param>
        /// 
        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToLower().Trim(); //trims white space and sets to lower case to avoid errors

            //return each shape based on shape type
            if (shapeType.Equals("rectangle"))
            {
                return new Rectangle();
            }
            else if (shapeType.Equals("line"))
            {
                return new Line();
            }
            else if (shapeType.Equals("triangle"))
            {
                return new Triangle();
            }
            else if (shapeType.Equals("circle"))
            {
                return new Circle();
            }
            else
            {
                //throw an exception if shapetype is not in factory
                throw new ArgumentException("Shape factory error:"+shapeType+"does not exist");
            }
        }
    }
}
