﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    class InvalidColourException : Exception
    {
        /// <summary>
        /// Custom invalid parameter exception
        /// </summary>
        /// This allows a custom exception to be thrown when the user attempts to change the pen colour, if the option is invalid, with a custom error message for the user.
        public InvalidColourException()
        {

        }

        public InvalidColourException(string message) : base(message)
        {
        }

        public InvalidColourException(string message, Exception inner) : base(message, inner)
        {
        }

        //overrides the exception message so it can be customised
        public override string Message
        {
            get
            {
                //returning the custom exception message
                return "Incorrect Colour provided. The available options are: Red, Blue, Yellow, Black";
            }
        }
    }
}
