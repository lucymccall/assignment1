﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace assignment1
{
    public class Triangle : Shape
    {

        /// <summary>
        /// Triangle shape
        /// </summary>
        /// <param name="point1">Triangle point 1</param>
        /// <param name="point2">Triangle point 2</param>
        /// <param name="point3">Triangle point 3</param>
        /// <param name="myBrush">Graphics brush</param>
        /// <param name="isfilled">bool for checking if fill is turned on</param>
        /// <param name="g">Graphics object</param>
        /// <param name="myPen">Graphics pen</param>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// This command derives from shape, so it has to use set and execute methods, as well as x and y coords.
        /// set is used to set the triangle points.
        /// Execute is used to draw the triangle using points from set method.


        //empty variables
        int point1, point2, point3;
        Graphics g;
        Pen myPen;
        Brush myBrush;
        bool isFilled;
        Drawing drawingclass;
        public Triangle() : base()
        {

        }

        //get the points
        public Triangle(int x, int y, int point1, int point2, int point3): base(x,y)
        {
            this.point1 = point1;
            this.point2 = point2;
            this.point3 = point3;
        }

        /// <summary>
        /// sets the correct params for triangle
        /// </summary>
        /// <param name="drawingclass">instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            try
            {
                base.Set(drawingclass, drawingclass.xPos, drawingclass.yPos); //sets the drawinglcass, x and y pos from base to correct values
                this.drawingclass = drawingclass; // sets local drawingclass to Drawing that was passed in
                this.g = Graphics.FromImage(drawingclass.myBitmap); //sets g to draw to bitmap from Drawing class

                //gets pen, brush and isfilled from drawing class and sets as local variables
                this.myPen = drawingclass.myPen;
                this.myBrush = drawingclass.myBrush;
                this.isFilled = drawingclass.isfilled;

                //if first param contains a user set variable, set its value as point1
                if (drawingclass.dict.ContainsKey((string)commands[0]))
                {
                    this.point1 = drawingclass.dict[(string)commands[0]];
                }
                else //else use int passed in
                {
                    this.point1 = Convert.ToInt32(commands[0]); //sets the point1 to value passed in through command list
                }

                //if second param contains a user set variable, set its value as point2
                if (drawingclass.dict.ContainsKey((string)commands[1]))
                {
                    this.point2 = drawingclass.dict[(string)commands[1]];
                }
                else //else use int passed in
                {
                    this.point2 = Convert.ToInt32(commands[1]); //sets the point2 to value passed in through command list
                }

                //if second param contains a user set variable, set its value as point3
                if (drawingclass.dict.ContainsKey((string)commands[1]))
                {
                    this.point3 = drawingclass.dict[(string)commands[1]];
                }
                else //else use int passed in
                {
                    this.point3 = Convert.ToInt32(commands[1]); //sets the point3 to value passed in through command list
                }
            } catch (Exception)
            {
                throw new InvalidParameterException();
            }

        }

        /// <summary>
        /// executes command
        /// </summary>
        public override void Execute()
        {
            //creates points array using variables from set
            Point[] points = { new Point(x, y), new Point(x + point1, y), new Point(x + point2, y + point3) }; //point1 = AB base length, point2 = vertice C x-axis, point3 = vertice C y-axis
            
            //if filled is true, draw filled triangle, else draw unfilled triangle
            if (isFilled == true)
            {
                g.FillPolygon(myBrush, points);
                //update pen coords
                drawingclass.xPos = point2 + drawingclass.xPos;
                drawingclass.yPos = point3 + drawingclass.yPos;

            } else
            {
                g.DrawPolygon(myPen, points);
                //update pen coords
                drawingclass.xPos = point2 + drawingclass.xPos;
                drawingclass.yPos = point3 + drawingclass.yPos;
            }
            
        }
    }
}
