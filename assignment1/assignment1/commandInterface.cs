﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    /// <summary>
    /// Create Commands Interface
    /// </summary>
    /// This interface is implimented by command class, so that all shapes and commands have to use 
    /// set and execute methods with the correct parameters passed.
    interface Createcommands
    {
        /// <summary>
        /// Set method
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        /// 
        /// Used to set the parameters needed for each command/shape
        void Set(Drawing drawingclass, params object[] commands); //sets the params for each shape/command

        /// <summary>
        /// Execute method
        /// </summary>
        /// Used to execute each command/shape
        void Execute(); //executes each shape/command
    }
}
