﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public class MoveTo : Command
    {
        /// <summary>
        /// MoveTo command
        /// </summary>
        /// <param name="newXPos">The new pen X coordinate</param>
        /// <param name="newYPos">The new pen Y coordinate</param>
        /// 
        /// Moves the pen to a new location on the bitmap.
        /// Uses set method to get the new position.
        /// execute method then sets this new position so its used for the next shapes.

        //empty variables
        int newXPos, newYPos;
        Drawing drawingclass;

        /// <summary>
        /// Set the variables for moveto
        /// </summary>
        /// <param name="drawingclass">instance of drawing class</param>
        /// <param name="commands">user inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            try
            {
                //set empty drawingclass to Drawing class thats created in form1.
                this.drawingclass = drawingclass;

                //if first param contains a user set variable, set its value as newXPos
                if (drawingclass.dict.ContainsKey((string)commands[0]))
                {
                    this.newXPos = drawingclass.dict[(string)commands[0]];
                }
                else //else use int that was passedin
                {
                    this.newXPos = Convert.ToInt32(commands[0]); //sets the newXPos to value passed in through command list
                }

                //if second param contains a user set variable, set its value as endY
                if (drawingclass.dict.ContainsKey((string)commands[1]))
                {
                    this.newYPos = drawingclass.dict[(string)commands[1]];
                }
                else //else use int that was passed in
                {
                    this.newYPos = Convert.ToInt32(commands[1]); //sets the newYPos to value passed in through command list
                }
            } catch (Exception)
            {
                throw new InvalidParameterException();
            }

        }

        /// <summary>
        /// execute the command
        /// </summary>

        public override void Execute()
        {
            //update the x and y pen coords
            drawingclass.xPos = newXPos;
            drawingclass.yPos = newYPos;
        }
    }
}
