﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public class SetVar :  Command
    {

        /// <summary>
        /// Adds new variables to a dictionary from Drawing class
        /// </summary>
        /// <param name="varname">The variable name</param>
        /// <param name="value">GThe variable value</param>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// 
        /// Adds the new variables using dict.add, so that they can be accessed later to be used in different shapes and commands.

        //empty variables
        string varname;
        int value;
        Drawing drawingclass;

        /// <summary>
        /// sets the correct params for setting a variable
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            try
            {
                //set local variables as passed in params
                this.varname = (string)commands[0];
                this.value = Convert.ToInt32(commands[1]);
                this.drawingclass = drawingclass;

            } catch (Exception)
            {
                throw new InvalidParameterException();
            }
        }

        /// <summary>
        /// Executes command
        /// </summary>
        public override void Execute()
        {
            //add the params to the dictionary
            if (!drawingclass.dict.ContainsKey("varname"))
            {
                drawingclass.dict.Add(varname, value);
            }
            else
            {
                throw new InvalidParameterException();
            }
            
        }
        
    }
}
