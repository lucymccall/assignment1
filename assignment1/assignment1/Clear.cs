﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace assignment1
{
    public class Clear : Command //impliments command
    {
        /// <summary>
        /// clear command
        /// </summary>
        /// <param name="g">Graphics object</param>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// 
        /// clears the bitmap fully and resets pen/brush
        /// Uses a Set method to take Drawing class and set graphics as bitmap
        /// Execute method uses graphics object to clear the bitmap


        //create empty graphics object
        Graphics g;
        Drawing drawingclass;

        /// <summary>
        /// Set the correct params for clear
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands) //set variables
        {
            //set empty graphics object as bitmap from drawing class
            this.g = Graphics.FromImage(drawingclass.myBitmap);
            this.drawingclass = drawingclass;
        }
        /// <summary>
        /// Execute the command
        /// </summary>
        public override void Execute()
        {
            g.Clear(Color.Transparent); //clears graphics and resets bitmap background to transparent

            //resets the pen, brush, and isfilled bool
            drawingclass.myPen.Color = Color.Black;
            drawingclass.myBrush = new SolidBrush(Color.Black);
            drawingclass.isfilled = false;

            //sets pen position back to 0,0
            drawingclass.xPos = 0;
            drawingclass.yPos = 0;
        }
    }
}
