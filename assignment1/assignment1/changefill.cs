﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public class changefill : Command
    {
        /// <summary>
        /// change fill option to on or off
        /// </summary>
        /// Execute method takes the following parameters:
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="isfilled">Bool for setting fill as true/false</param>
        /// 
        /// Checks if bool isfilled is equal true or false, and changes Drawing class isfilled bool respectively.


        //empty variables
        Drawing drawingclass;
        bool isfilled;

        /// <summary>
        /// set variables
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            this.drawingclass = drawingclass; //set empty drawingclass to Drawing
            this.isfilled = Convert.ToBoolean(commands[0]); //convert object to boolean and sets as local isfilled
        }

        /// <summary>
        /// execute command
        /// </summary>
        public override void Execute()
        {
            if (isfilled == true) //checks if fill is equal true
            {
                drawingclass.isfilled = true; //change isfilled to true
            }
            else if (isfilled == false) //checks if fill is equal to false
            {
                drawingclass.isfilled = false; //change is filled to false
            }
        }
    }
}
