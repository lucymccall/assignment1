﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public class ResetPen : Command
    {
        /// <summary>
        /// Reset the pen to 0
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>

        //empty variables
        Drawing drawingclass;

        /// <summary>
        /// sets the correct params for resetpen
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            try
            {
                //set empty drawingclassto Drawing thats passed in
                this.drawingclass = drawingclass;
            } catch (Exception)
            {
                throw new InvalidParameterException();
            }

        }
        /// <summary>
        /// executes command
        /// </summary>
        public override void Execute()
        {
            //set x and y to 0
            drawingclass.xPos = 0;
            drawingclass.yPos = 0;
        }
    }
}
