﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace assignment1
{
    public class Rectangle : Shape
    {

        /// <summary>
        /// Rectangle shape
        /// </summary>
        /// <param name="width">width of circle</param>
        /// <param name="height">height of circle</param>
        /// <param name="g">Graphics object</param>
        /// <param name="myPen">Graphics pen</param>
        /// <param name="myBrush">Graphics brush</param>
        /// <param name="isfilled">bool for checking if fill is turned on</param>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// 
        /// This command derives from shape, so it has to use set and execute methods, as well as x and y coords.
        /// set is used to set the width and height of rectangle, and the elements needed for drawing.
        /// Execute is used to draw the rectangle using the variables that were set.

        //empty variables
        int width, height;
        Graphics g;
        Pen myPen;
        Brush myBrush;
        bool isFilled;
        Drawing drawingclass;

        public Rectangle():base()
        {
            
        }

        //gets x and y from base and sets local width/height to equal width and height.
        public Rectangle(int x, int y, int width, int height): base(x,y)
        {
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// sets the correct params for rectangle
        /// </summary>
        /// <param name="drawingclass">instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            try
            {
                base.Set(drawingclass, drawingclass.xPos, drawingclass.yPos); //sets the drawinglcass, x and y pos from baseto correct values
                this.drawingclass = drawingclass; // sets local drawingclass to Drawing that was passed in
                this.g = Graphics.FromImage(drawingclass.myBitmap); //sets g to draw to bitmap from Drawing class

                //gets pen, brushed and isfilled from drawing class and sets as local variables
                this.myBrush = drawingclass.myBrush;
                this.isFilled = drawingclass.isfilled;
                this.myPen = drawingclass.myPen;

                //if first param contains a user set variable, set its value as width
                if (drawingclass.dict.ContainsKey((string)commands[0]))
                {
                    this.width = drawingclass.dict[(string)commands[0]];
                }
                else //else use int passed in
                {
                    this.width = Convert.ToInt32(commands[0]); //sets the width to value passed in through command list
                }

                //if second param contains a user set variable, set its value as height
                if (drawingclass.dict.ContainsKey((string)commands[1]))
                {
                    this.height = drawingclass.dict[(string)commands[1]];
                }
                else //else use int passed in
                {
                    this.height = Convert.ToInt32(commands[1]); //sets the height to value passed in through command list
                }
            } catch (Exception)
            {
                throw new InvalidParameterException();
            }

        }

        /// <summary>
        /// Executes command
        /// </summary>
        public override void Execute()
        {
            //if isfilles is tru, draw filled rectangle, else draw unfilled rectangle
            if (isFilled == true)
            {
                g.FillRectangle(myBrush, x, y, width, height);
                //update pen coords
                drawingclass.xPos = width + drawingclass.xPos;
                drawingclass.yPos = height + drawingclass.yPos;
            } 
            else
            {
                g.DrawRectangle(myPen, x, y, width, height);
                //update pen coords
                drawingclass.xPos = width + drawingclass.xPos;
                drawingclass.yPos = height + drawingclass.yPos;
            }
        }
    }
}
