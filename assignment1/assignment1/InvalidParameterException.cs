﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    class InvalidParameterException : Exception
    {
        /// <summary>
        /// Custom invalid parameter exception
        /// </summary>
        /// This allows a custom exception to be thrown if parameters are invalid, with a custom message for the user.
        public InvalidParameterException()
        {

        }

        public InvalidParameterException(string message) : base(message)
        {
        }

        public InvalidParameterException(string message, Exception inner) : base(message,inner)
        {
        }
        
        //overrides the exception message so it can be customised
        public override string Message
        {
            get
            {
                //returning the custom exception message
                return "Incorrect Parameters provided for the shape";
            }
        }
    }
}
