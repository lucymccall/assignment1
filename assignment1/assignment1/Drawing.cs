﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Data.SqlTypes;
using System.Windows.Forms;
using System.ComponentModel.Design;

namespace assignment1
{
    public class Drawing
    {
        /// <summary>
        /// Drawing class stores everything needed for drawing graphics by other classes
        /// </summary>
        /// Since all this information is in one class, it can be easily referenced throughout the
        /// program by passing the same instance through different methods. This instance is created in Form1.
         

        //Store all graphics related variables and objects
        public Brush myBrush = new SolidBrush(Color.Black); //paints a filled in shape
        public Pen myPen;
        public bool isfilled = false; // bool for filling shapes
        public int xPos, yPos; //pen position
        public Graphics g; //graphics object
        public Bitmap myBitmap = new Bitmap(570, 340); //The bitmap for drawing, this is used by almost every command/shape.

        //this dictionary holds the custom variables that are set by user in the program textbox within form1.
        public Dictionary<string, int> dict = new Dictionary<string, int>();

        /// <summary>
        /// Drawing constructor, so an instance of the class can be created from elsewhere
        /// </summary>
        /// Sets the pen colour as black when the instance is created
        public Drawing()
        {
            //sets the pen to black when the instance of drawing is created, otherwise would be null.
            myPen = new Pen(Color.Black);
        }
    }
}
