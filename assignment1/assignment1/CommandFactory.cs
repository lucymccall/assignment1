﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public class CommandFactory
    {
        /// <summary>
        /// The command factory
        /// </summary>
        /// This class returns the objects that are used in parser class to set and execute each command.
        /// getCommand method is used with the command type, in order to return that specific command.
        /// <param name="CommandType">The type of command</param>
        /// 
        public Command getCommand(string CommandType)
        {
            CommandType = CommandType.ToLower().Trim(); //trims white space and sets to lower case to avoid errors

            //return each command based on command type
            if (CommandType.Equals("clear"))
            {
                return new Clear();
            }
            else if (CommandType.Equals("changecolour"))
            {
                return new ChangeColour();
            }
            else if (CommandType.Equals("changefill"))
            {
                return new changefill();
            }
            else if (CommandType.Equals("setvar"))
            {
                return new SetVar();
            }
            else if (CommandType.Equals("moveto"))
            {
                return new MoveTo();
            }
            else if (CommandType.Equals("updatevar"))
            {
                return new UpdateVar();
            }
            else
            {
                //throw an exception if command type is not in factory
                throw new ArgumentException("Command factory error:" + CommandType + "does not exist");
            }
        }
    }
}
