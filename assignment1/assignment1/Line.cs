﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace assignment1
{
    public class Line : Shape
    {
        /// <summary>
        /// Line shape
        /// </summary>
        /// <param name="endX">The x coordinate of the end of the line</param>
        /// <param name="endY">The y coordinate of the end of the line</param>
        /// <param name="g">Graphics object</param>
        /// <param name="myPen">Graphics pen</param>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// 
        /// This command derives from shape, so it has to use set and execute methods, as well as x and y coords.
        /// set is used to set the end point of the line.
        /// Execute is used to draw the line using he endpoints from set method.


        //empty variables
        int endX, endY;
        Graphics g;
        Pen myPen;
        Drawing drawingclass;
        public Line() : base()
        {

        }
        //gets x and y from base and sets local endx/endy to equal endx and endy.
        public Line(int x, int y, int endX, int endY) : base(x,y)
        {
            this.endX = endX;
            this.endY = endY;
        }

        /// <summary>
        /// sets the correct params for line
        /// </summary>
        /// <param name="drawingclass">drawing class instance</param>
        /// <param name="commands">user inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            try
            {
                base.Set(drawingclass, drawingclass.xPos, drawingclass.yPos); //sets the drawinglcass, x and y pos from base to correct values
                this.g = Graphics.FromImage(drawingclass.myBitmap); //sets g to draw to bitmap from Drawing class
                this.drawingclass = drawingclass; // sets local drawingclass to Drawing that was passed in
                this.myPen = drawingclass.myPen; //gets pen from drawing class and sets as local pen

                //if first param contains a user set variable, set its value as endX
                if (drawingclass.dict.ContainsKey((string)commands[0]))
                {
                    this.endX = drawingclass.dict[(string)commands[0]];
                }
                else //else use normal endX
                {
                    this.endX = Convert.ToInt32(commands[0]); //sets the radius to value passed in through command list
                }

                //if second param contains a user set variable, set its value as endY
                if (drawingclass.dict.ContainsKey((string)commands[1]))
                {
                    this.endY = drawingclass.dict[(string)commands[1]];
                }
                else //else use normal endY
                {
                    this.endY = Convert.ToInt32(commands[1]); //sets the radius to value passed in through command list
                }
            } catch (Exception)
            {
                throw new InvalidParameterException();
            }


        }

        /// <summary>
        /// Execute command
        /// </summary>
        public override void Execute(
        {
            //draw line to bitmap
            g.DrawLine(myPen, x, y, x + endX, y + endY);

            //update x and y pos
            drawingclass.xPos = endX + drawingclass.xPos;
            drawingclass.yPos = endY + drawingclass.yPos;
        }
    }
}
