﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace assignment1
{
    public abstract class Command:Createcommands
    {
        /// <summary>
        /// Command class that all commands derive from
        /// </summary>
        /// All commands and shape classes derive from this class, as they all need the same set and 
        /// execute methods with very similar parameters.
        public Command() {}

        /// <summary>
        /// Set method
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        /// 
        /// Used to set the parameters needed for each command/shape
        public abstract void Set(Drawing drawingclass, params object[] commands);

        /// <summary>
        /// Execute method
        /// </summary>
        /// Used to execute each command/shape
        public abstract void Execute();

    }
}
