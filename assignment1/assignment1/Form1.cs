﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace assignment1
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// This is the main form that the user uses to navigate the program
        /// </summary>
        /// This is used to draw each component of the form, and to pass the textboxes through to
        /// the parser class so the correct commands and shapes can be executed and drawn onto the bitmap.
         

        //creating a new of of Drawing class,to be passed into parser.
        Drawing DrawingClass = new Drawing();
        

        String Action; //where contents of commandline are stored
        String Program; //where contents of program textbox are stored

        public Form1()
        {
            //initialises form1 elements so they are drawn onto the form when program is ran.
            InitializeComponent();
        }

        /// <summary>
        /// The keydown event listener method
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">The keydown event listener</param>
        /// 
        ///Listens for the enter key to be pressed, and then processes the contents of the command line to call various methods
        private void tbCMD_KeyDown(object sender, KeyEventArgs e)
        {
          
            // activated on enter keypress of commandline textbox
            if (e.KeyCode == Keys.Enter)
            {
                //create object of parser class and pass Drawing class into it that was created at top of form.
                Parser parse = new Parser(DrawingClass);

                // gets the text from textboxes, trims white space and makes all lowercase
                this.Program = tbProgram.Text.Trim().ToLower();
                this.Action = tbCMD.Text.Trim().ToLower();

                //if action contains save, then the program is saved to file system.
                if (Action.Contains("save") == true)
                {
                    try
                    {
                        File.WriteAllText("program.txt", tbProgram.Text); //write program textbox contents to txt file and save to filesystem as 'program.txt'
                        tbProgram.Clear(); // clear the program textbox after saving
                    }
                    catch (Exception ex) //catch the exception if file cant be saved
                    {
                        //Message shows to user on screen and in console
                        MessageBox.Show("The file could not be read");
                        Console.WriteLine(ex.Message);
                    }  
                }

                //if action contains load, then the program is loaded from file system.
                else if (Action.Contains("load") == true)
                {
                    //attemps to load the file and handles any errors
                    try
                    {
                        if (File.Exists("program.txt")) // check if filename exists in filesystem
                        {
                            tbProgram.Text = File.ReadAllText("program.txt"); //set program textbox as file contents
                        }
                    }

                    catch (Exception ex) // catches exception if file cant be loaded
                    {
                        //Message shows to user on screen and in console
                        MessageBox.Show("The file could not be read");
                        Console.WriteLine(ex.Message);
                    }
                }

                //if action equals run, then send program to program parser in parseclass
                else if (Action.Equals("run"))
                {
                    parse.parseProgram(Program);
                } 

                //if action doesnt equal anything else then send the action through to action parser in parse class
                else {
                    parse.ParseAction(Action);
                }
                tbCMD.Clear(); // clear commandline after every command
                Refresh(); //redraw the form on each keypress, to show graphics
            }
        }
        /// <summary>
        /// Draws the bitmap to the picturebox
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Paint event</param>
        private void DrawArea_Paint(object sender, PaintEventArgs e)
        {
            Bitmap myBitmap = DrawingClass.myBitmap; //sets local bitmap as the bitmap from Drawing class.
            Graphics g = e.Graphics; // set graphics object as painteventargs
            g.DrawImageUnscaled(myBitmap, 0, 0); // draw bitmap onto picturebox
        }
    }

}
