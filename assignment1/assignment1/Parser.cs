﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace assignment1
{
    public class Parser
    {

        /// <summary>
        /// Parse class, used to parse the actions and program contents from form1
        /// </summary>
        /// Creates instances of command and shape factory so objects can be created and executed in the parseaction and parseprogram class.
        /// <param name="DrawingClass">Instance of drawing class</param>
        /// <param name="commandfactory">Instance of the command factory</param>
        /// <param name="shapefactory">Instance of the shape factory</param>
        /// <param name="loopcount">Holds the amount of times the user wants to do a loop</param>
        /// <param name="Testing">bool for setting testing as true if unit tests are running, so graohics are not ran in this case</param>

        //create Drawing class and factories
        Drawing DrawingClass;
        CommandFactory commandfactory = new CommandFactory();
        ShapeFactory shapefactory = new ShapeFactory();

        //empty loopcount int for settign how many times user wants to loop
        int loopcount;
        bool Testing = false;

        //Takes param of Drawing, so it can be passed in from form1 to be used throughout parser class
        public Parser(Drawing drawingclass)
        {
            this.DrawingClass = drawingclass;
        }

        public Parser()
        {
            this.Testing = true;
        }

        /// <summary>
        /// Parses the action from commandline or parseAction method
        /// </summary>
        /// <param name="Action">Commandline contents from form1 or parseAction method</param>
        /// Checks what the command is, and creates an object of the shape/command using user inputted commands.
        /// Executes the created command object so the bitmap and graphics get updated
        public void ParseAction(string Action)
        {
            //Create list of objects from action thats passed in
            object[] Actions = Action.Split(' ');
            //creates from first element in array
            string command = (string)Actions[0];


            //if statement is true and command is equal to one of the following, its command or shape is 
            //set and executed from factory with the params passed in by the user
            if (command.Equals("colour"))
            {
                try
                {
                    //create object from factory, set params and execute
                    ChangeColour c = (ChangeColour)commandfactory.getCommand("changecolour");
                    c.Set(DrawingClass, (string)Actions[1]);
                    c.Execute();
                }
                catch (InvalidParameterException e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                    ErrorMessage("Incorrect colour:" + e);
                }
            }
            else if (command.Equals("line"))
            {
                try
                {
                    //create object from factory, set params and execute
                    Line L = (Line)shapefactory.getShape("line");
                    L.Set(DrawingClass, Actions[1], Actions[2]);
                    L.Execute();
                }
                catch (Exception e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                    ErrorMessage("incorrect params for line");
                }
   
            }
            else if (command.Equals("rectangle")) 
            {
                try
                {
                    //create object from factory, set params and execute
                    Rectangle R = (Rectangle)shapefactory.getShape("rectangle");
                    R.Set(DrawingClass, Actions[1], Actions[2]);
                    R.Execute();


                }
                catch (Exception e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                    ErrorMessage("incorrect params for rectangle");
                }
                
            }
            else if (command.Equals("triangle"))
            {
                try
                {
                    //create object from factory, set params and execute
                    Triangle T = (Triangle)shapefactory.getShape("triangle");
                    T.Set(DrawingClass, Actions[1], Actions[2], Actions[3]);
                    T.Execute();
                }
                catch (Exception e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                    ErrorMessage("incorrect params for triangle");
                }
                
            }
            else if (command.Equals("circle"))
            {
                try
                {
                    //create object from factory, set params and execute
                    Circle T = (Circle)shapefactory.getShape("circle");
                    T.Set(DrawingClass, Actions[1]);
                    T.Execute();
                }
                catch (Exception e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                    ErrorMessage("incorrect params for circle" + e);
                }
            }
            else if (command.Equals("fill"))
            {
                try
                {
                    //get second item in array and set as fill string
                    string fill = (string)Actions[1];

                    //if fill is equal to on, execute
                    if (fill.Contains("on"))
                    {
                        //create object from factory, set params and execute
                        changefill c = (changefill)commandfactory.getCommand("changefill");
                        c.Set(DrawingClass, true);
                        c.Execute();
                    } 
                    //if fill is equal to off, execute
                    else if (fill.Contains("off")){
                        //create object from factory, set params and execute
                        changefill c = (changefill)commandfactory.getCommand("changefill");
                        c.Set(DrawingClass, false);
                        c.Execute();
                    } 
                }
                catch (Exception e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                    ErrorMessage("incorrect params for circle: " + e);
                }
            }
            else if (command.Equals("clear")) {
                try
                {
                    //create object from factory, set params and execute
                    Clear c = (Clear)commandfactory.getCommand("clear");
                    c.Set(DrawingClass);
                    c.Execute();

                }
                catch (Exception e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                    ErrorMessage("incorrect command");
                }
            }
            else if (command.Equals("moveto"))
            {
                try
                {
                    //create object from factory, set params and execute
                    MoveTo m = (MoveTo)commandfactory.getCommand("moveto");
                    m.Set(DrawingClass, Actions[1], Actions[2]);
                    m.Execute();
                }
                catch (Exception e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                    ErrorMessage("incorrect params for moveto: " + e);
                }
            }
            else if (command.Equals("set"))
            {
                try
                {
                    //create object from factory, set params and execute
                    SetVar s = (SetVar)commandfactory.getCommand("setvar");
                    s.Set(DrawingClass, Actions[1], Actions[2]);
                    s.Execute();
                }
                catch (Exception e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                    ErrorMessage("incorrect params for setting variable: " + e);

                }
                
            } else if (command.Equals("update"))
            {
                try
                {
                    //create object from factory, set params and execute
                    UpdateVar s = (UpdateVar)commandfactory.getCommand("updatevar");
                    s.Set(DrawingClass, Actions[1], Actions[2], Actions[3]);
                    s.Execute();
                }
                catch (Exception e) //catch any errors that are thrown and show a message to user
                {
                    Console.WriteLine(e);
                   // ErrorMessage("incorrect params for updating variable: " + e);

                }
            }
            else
            {
                if (Testing == false)
                {
                    ErrorMessage("Incorrect command");
                }
                throw new ArgumentException();
                
            }

        }
        /// <summary>
        /// Parse Program
        /// </summary>
        /// <param name="program"></param>
        /// this takes a param of program, and splits it into an array by newline.
        /// A foreach loop is then executed to iterate through each item, checking for loops and commands.
        /// If a loop is found, the loop is removed from the program after execution, and a continue statement 
        /// is used to move on to the next item in the array.
        public void parseProgram(string program)
        {
            //split by newline
            string[] lines = program.Split('\n');

            //Iterate through each item in array
            foreach (string a in lines)
            {
                //if the line contains loop, then do the following
                if (a.Contains("loop"))
                {
                    //get the start and end point of the loop
                    int start = program.IndexOf("loop") + 4; // + 4 for the length of the word and one space
                    int end = program.IndexOf("endloop");

                    //if start and end are greater then 0, do the following
                    if (start > 0 && end > 0)
                    {
                        //retrieve the contents of the for loop
                        string loop = program.Substring(start, end - start);
                        // end - start, because Substring takes the start index, and
                        // the "length" of how much text you want to extract, not end index

                        //create list of items in the loop, so they czan be iterated through
                        List<string> looplines = loop.Split('\n').ToList();

                        //set how many times user wants to loop through
                        try
                        {
                            this.loopcount = int.Parse(looplines[0]);
                        }
                        catch (FormatException e)
                        {
                            if (Testing == false)
                            {
                                ErrorMessage("Incorrect format for loop count: " + e);
                            }
                            throw new FormatException();
                        }

                        //remove the first and last item of the list, as these arent being used now and would cause errors
                        looplines.RemoveAt(0);
                        looplines.RemoveAt(looplines.Count - 1);
                        if (Testing == false)
                        {
                            //iterate through each command in the list
                            foreach (string l in looplines)
                            {
                                //do each line the amount of times specified by user
                                for (int i = 0; i <= loopcount - 2; i++)
                                {
                                    try
                                    {
                                        //send line to action parser
                                        ParseAction(l);

                                    }
                                    catch (Exception) //catch any errors thrown while looping through and display an error message
                                    {
                                        ErrorMessage("Incorrect command at line:" + l);
                                    }
                                }
                            }
                        }

                        //remove the loop from the program
                        program = program.Remove(start - 4, end + 7);
                    }
                    //continue to the next item in loop
                    continue;
                }
                else //if the line doesnt equal loop, then send to parseaction class as normal
                {
                    try
                    {
                        ParseAction(a);
                    }
                    catch (Exception) //catch any errors thrown while looping through and display an error message
                    {
                        if (Testing == false)
                        {
                            ErrorMessage("Found at line: " + a);
                        }

                        //throw an argument exception
                        throw new ArgumentException();
                    }
                }    
            }
        }

        /// <summary>
        /// Used to easily display error in a messagebox for user
        /// </summary>
        /// <param name="Message">The messages to shop up in the messagebox</param>
        public void ErrorMessage(string Message)
        {
            MessageBox.Show(Message);
        }
    }
}
