﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace assignment1
{
    public class Circle : Shape
    {
        /// <summary>
        /// Circle object
        /// </summary>
        /// <param name="radius">Radius of circle</param>
        /// <param name="g">Graphics object</param>
        /// <param name="myPen">Graphics pen</param>
        /// <param name="myBrush">Graphics brush</param>
        /// <param name="isfilled">bool for checking if fill is turned on</param>
        /// 
        /// This class allows the factory circle object to be created with the correct params, and executed easily.
        /// Set method allows for variables and params to be set, so that the circle object thats created has values.
        /// Execute method can then be called to impliment the circle object, and draw to bitmap.

        //create empty variables
        int radius;
        Graphics g;
        Pen myPen;
        Brush myBrush;
        bool isFilled;

        public Circle() : base()
        {

        }

        //gets x and y from base and sets local radius var to equal radius
        public Circle(int x, int y, int radius) : base(x,y)
        {
            this.radius = radius;
        }

        /// <summary>
        /// sets the correct params for circle
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            try
            {
                base.Set(drawingclass, drawingclass.xPos, drawingclass.yPos); //sets the drawinglcass, x and y pos from baseto correct values
                this.g = Graphics.FromImage(drawingclass.myBitmap); //sets g to draw to bitmap from Drawing class
                                                                    //gets pen, brush and isfilled from drawing class and sets them as circles local variables
                this.myPen = drawingclass.myPen;
                this.myBrush = drawingclass.myBrush;
                this.isFilled = drawingclass.isfilled;

                //if command contains a user set variable, set its value as radius
                if (drawingclass.dict.ContainsKey((string)commands[0]))
                {
                    this.radius = drawingclass.dict[(string)commands[0]];
                }
                else //else use normal radius
                {
                    this.radius = Convert.ToInt32(commands[0]); //sets the radius to value passed in through command list
                }
            } catch (Exception)
            {
                throw new InvalidParameterException();
            }
        }

        /// <summary>
        /// Executes command
        /// </summary>
        public override void Execute() //execute the circle to draw to bitmap
        {
            if (isFilled == true) //check to see if isfilled is true
            {
                //draw a filled ellipse using variables from Set method
                g.FillEllipse(myBrush, x - radius, y - radius, radius + radius, radius + radius);  
            }
            else
            {
                //draw an unfilled ellipse using variables from set method
                g.DrawEllipse(myPen, x - radius, y - radius, radius + radius, radius + radius);
            }
        }
    }
}
