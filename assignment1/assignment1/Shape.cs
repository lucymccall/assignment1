﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace assignment1
{
    public class Shape:Command
    {
        /// <summary>
        /// Shape class
        /// </summary>
        /// <param name="x">pen x coord</param>
        /// <param name="y">pen y coord</param>
        /// 
        /// All shapes derive from this class. They use the set and execute methods to draw
        /// and refer to this base class when setting x and y coords.

        //empty variables
        protected int x, y;
        public Shape()
        {
            
        }

        //set x and y
        public Shape(int xPos, int yPos){
            this.x = xPos;
            this.y = yPos;
        }

        /// <summary>
        /// sets the correct params for shape
        /// </summary>
        /// <param name="drawingclass">Instance of drawing class</param>
        /// <param name="commands">User inputted commands</param>
        public override void Set(Drawing drawingclass, params object[] commands)
        {
            this.x = (int)commands[0];
            this.y = (int)commands[1];
        }

        /// <summary>
        /// Executes command
        /// </summary>
        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
